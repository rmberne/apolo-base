<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<jsp:include page='/WEB-INF/views/menu/menu.jsp'></jsp:include>

<!-- Subhead ================================================== -->
<header id="header">
  <div class="container">
    <p class="lead">&nbsp;</p>
  </div>
</header>

<jsp:include page='/WEB-INF/views/include/breadcrumb.jsp'></jsp:include>
